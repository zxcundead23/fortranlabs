
program hello
    implicit none
    real:: x, k, eps, y0, yn, d, p, l, y1, yh, d1, p1, l1
    integer::i, n, perv, vtor
    read(*,*) eps
    read(*,*) n
    read(*,*) x
    read(*,*) k
    y0 = 2/(x + (k - 1))
    yn = y0
    do i = 1,n
        y0 = yn
        yn = (yn*(k + 1 - x * (yn ** k)))/k
        d = abs((yn**k)*x - 1)
        if ((d <= eps)) then
            p = d
            l = yn
            perv = i
            exit
        end if
    end do
    y1 = 2/(x + (k - 1))
    yh = y1
    do i = 1,n
        y1 = yh
        yh = ((k-1)*yh + 1/(x * (yh**(k-1))))/k
        d1 = abs((yh**k)*x - 1)
        if (d1 <= eps) then
            p1 = d1
            l1 = yh
            vtor = i
            exit
        end if
    end do
    if (perv < vtor) then
        print*, "Lagrange:", perv, p, l
    else
        print*, "Newton:", vtor, p1, l1
    end if

end program

