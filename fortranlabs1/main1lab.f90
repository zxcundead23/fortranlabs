
program hello
    implicit none
    real:: after
    integer:: time, now
    read(*,*) now
    after = now
    time = 0
    do while ((after/real(now)) <= 2.0)
        time = time + 1
        write(*,*) time
        after = after * (1 + (1/80.0))
    end do
    write(*,*) after, time
end program hello

