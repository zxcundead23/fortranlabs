
program hello
    implicit none
    integer:: n
    real:: x, y
    read(*,*) n
    read(*,*) x
    if (n.eq. 1) then
        y = cos(x)
        write(*,*) y
    end if
    if (n == 2) then
        y = 2 * cos(x)**2 - 1
        print *, y
    end if
    if (n == 3) then
        y = 4*cos(x)**3 - 3*cos(x)
        print *, y
    end if
    if (n == 4) then
        y = 8*cos(x)**4 - 8*cos(x)**2 + 1
        write(*,*) y
    end if
    if (n == 5) then
        y = 16*cos(x)**5 - 20*cos(x)**3 + 5*cos(x)
        write(*,*) y
    end if
    if ((n>5) .or. (n<1)) then
        print*, "write number(1:5)"
    end if

end program hello
