
program hello
    implicit none
    integer:: g, l
    real:: z, c, m
    integer:: n, i
    real:: a(13), b(13)
    read(*,*) a(1:13)
    n = 13
    b(1) = a(1)
    b(n) = a(n)
    c = a(1)
    m = b(1)
    do i = 2,(n)
        z = (a(i-1) + a(i) + a(i+1))/3
        b(i) = z
        write(*,*) z
        if (a(i) > c) then
            c = a(i)
            g = i
        end if
        if (b(i) > m) then
            m = b(i)
            l = i
        end if
    end do
    if (a(n) > c) then
        c = a(n)
    end if
    write(*,*) c, m, g, l
    if (m > c) then
        write(*,*) b
    else
        write(*,*) c
    end if


end program

