
program main

    implicit none

    real :: x0, xk, h, eps
    real :: xi, y, Un, Sn
    integer :: i, n, ni, Nmax
    integer, parameter :: imax = 10000

    ! ���� ������ � ����������
    write(*,*) 'Enter Nmax'
    read(*,*) Nmax
    write(*,*) 'Enter x0:'
    read(*,*) x0
    write(*,*) 'Enter xk:'
    read(*,*) xk
    write(*,*) 'Enter h:'
    read(*,*) h

    eps = 1e-7

    ! �������� �� ����� �� ��������
    if (x0 < -1.0 .or. xk > 1.0) then
        write(*,*) 'Arguments out of range'
        stop
    endif

    ! ����� ��������� �������
    write(*,'(A15,A15,A15,A15)') 'i', 'xi', 'ni', 'yi'

    do i = 1, imax

        xi = x0 + (i-1)*h

        ! �������� �� ����� �� ��������
        if (xi < -1.0 .or. xi > 1.0) then
            xi = -1.0 + eps
        endif

        ! ���������� �������� �������
        y = 1.0

        if (abs(xi) < 1.0) then

            Un = xi
            Sn = xi

            do n = 1, Nmax

                Un = -Un * (2*n - 1) * (2*n - 1) / ((2*n) * (2*n + 1)) * xi**2
                Sn = Sn + Un

                if (abs(Un) <= eps * abs(Sn)) then
                    ni = n
                    exit
                endif

                y = 1.0 + Sn

            end do

        endif

        ! ����� ������ �������
        write(*,'(I15,F15.6,I15,F15.6)') i, xi, ni, y

        ! ��������� ����� ��� ���������� �������� ����� xk
        if (xi >= xk) exit

    end do

end program main
